package xzh.hospital.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xzh.hospital.model.order.OrderInfo;
import xzh.hospital.vo.order.OrderCountQueryVo;
import xzh.hospital.vo.order.OrderCountVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper extends BaseMapper<OrderInfo> {

    //查询预约统计数据的方法
    List<OrderCountVo> selectOrderCount(@Param("vo") OrderCountQueryVo orderCountQueryVo);
}
