package xzh.hospital.hospitalmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xzh.hospital.hospitalmanage.model.HospitalSet;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {

}
