import request from '@/utils/request'

export default {
  // 医院列表
  getHospList(page, limit, serchObj) {
    return request({
      url: `/admin/hosp/hospital/list/${page}/${limit}`,
      method: 'get',
      params: serchObj
    })
  },
  // 根据dictcode查询所有子节点（所有省）
  findByDictCode(dictCode) {
    return request({
      url: `/admin/cmn/dict/findByDictCode/${dictCode}`,
      method: 'get'
    })
  },
  // 根据id查询下级字典数据
  findChildId(dictCode) {
    return request({  
      url: `/admin/cmn/dict/findChildData/${dictCode}`,
      method: 'get'
    })
  },
  // 更新医院状态
  updateStatus(id, status) {
    return request({  
      url: `/admin/hosp/hospital/updateHospStatus/${id}/${status}`,
      method: 'get'
    })
  },
  // 查看医院详情
  getHospById(id) {
    return request({  
      url: `/admin/hosp/hospital/showHospDetail/${id}`,
      method: 'get'
    })
  },
  getScheduleByHoscode(hoscode){
    return request ({
      url: `/admin/hosp/department/getDeptList/${hoscode}`,
      method: 'get'
    })
   },
    getScheduleRule(page, limit, hoscode, depcode) {
      return request({
       url: `/admin/hosp/schedule/getScheduleRule/${page}/${limit}/${hoscode}/${depcode}`,
       method: 'get'
      })
   },
     getScheduleDetail(hoscode,depcode,workDate) {
      return request ({
        url: `/admin/hosp/schedule/getScheduleDetail/${hoscode}/${depcode}/${workDate}`,
        method: 'get'
      })
    }
}
