package xzh.hospital.mapper;

import xzh.hospital.model.hosp.Schedule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface ScheduleMapper extends BaseMapper<Schedule> {
}
