package xzh.hospital.hospitalmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xzh.hospital.hospitalmanage.model.Schedule;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScheduleMapper extends BaseMapper<Schedule> {

}
