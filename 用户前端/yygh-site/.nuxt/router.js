import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _352042f5 = () => interopDefault(import('..\\pages\\order\\index.vue' /* webpackChunkName: "pages/order/index" */))
const _68df949e = () => interopDefault(import('..\\pages\\patient\\index.vue' /* webpackChunkName: "pages/patient/index" */))
const _3fe9b706 = () => interopDefault(import('..\\pages\\user\\index.vue' /* webpackChunkName: "pages/user/index" */))
const _76ba42d4 = () => interopDefault(import('..\\pages\\hosp\\booking.vue' /* webpackChunkName: "pages/hosp/booking" */))
const _375951ac = () => interopDefault(import('..\\pages\\hosp\\schedule.vue' /* webpackChunkName: "pages/hosp/schedule" */))
const _208de92a = () => interopDefault(import('..\\pages\\order\\show.vue' /* webpackChunkName: "pages/order/show" */))
const _7b2a61ed = () => interopDefault(import('..\\pages\\patient\\add.vue' /* webpackChunkName: "pages/patient/add" */))
const _22393e61 = () => interopDefault(import('..\\pages\\patient\\show.vue' /* webpackChunkName: "pages/patient/show" */))
const _f38ea350 = () => interopDefault(import('..\\pages\\weixin\\callback.vue' /* webpackChunkName: "pages/weixin/callback" */))
const _51dceda0 = () => interopDefault(import('..\\pages\\hosp\\detail\\_hoscode.vue' /* webpackChunkName: "pages/hosp/detail/_hoscode" */))
const _2dbaf3e7 = () => interopDefault(import('..\\pages\\hosp\\notice\\_hoscode.vue' /* webpackChunkName: "pages/hosp/notice/_hoscode" */))
const _c67a8ee2 = () => interopDefault(import('..\\pages\\hosp\\_hoscode.vue' /* webpackChunkName: "pages/hosp/_hoscode" */))
const _4fa65c71 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/order",
    component: _352042f5,
    name: "order"
  }, {
    path: "/patient",
    component: _68df949e,
    name: "patient"
  }, {
    path: "/user",
    component: _3fe9b706,
    name: "user"
  }, {
    path: "/hosp/booking",
    component: _76ba42d4,
    name: "hosp-booking"
  }, {
    path: "/hosp/schedule",
    component: _375951ac,
    name: "hosp-schedule"
  }, {
    path: "/order/show",
    component: _208de92a,
    name: "order-show"
  }, {
    path: "/patient/add",
    component: _7b2a61ed,
    name: "patient-add"
  }, {
    path: "/patient/show",
    component: _22393e61,
    name: "patient-show"
  }, {
    path: "/weixin/callback",
    component: _f38ea350,
    name: "weixin-callback"
  }, {
    path: "/hosp/detail/:hoscode?",
    component: _51dceda0,
    name: "hosp-detail-hoscode"
  }, {
    path: "/hosp/notice/:hoscode?",
    component: _2dbaf3e7,
    name: "hosp-notice-hoscode"
  }, {
    path: "/hosp/:hoscode?",
    component: _c67a8ee2,
    name: "hosp-hoscode"
  }, {
    path: "/",
    component: _4fa65c71,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
