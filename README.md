# 医院预约挂号

#### 介绍
基于SpringCloud的前后端分离的医院预约挂号平台

#### 使用技术
SpringBoot：简化新Spring应用的初始搭建以及开发过程

SpringCloud：基于Spring Boot实现的云原生应用开发工具，SpringCloud使用的技术：（SpringCloudGateway、Spring Cloud Alibaba Nacos、Spring Cloud Alibaba Sentinel、SpringCloud Task和SpringCloudFeign等）

MyBatis-Plus：持久层框架

Redis：内存缓存

RabbitMQ：消息中间件

Swagger2：Api接口文档工具

Nginx：负载均衡

Lombok

Mysql：关系型数据库

Vue.js：web 界面的渐进式框架

Node.js： JavaScript 运行环境

Axios：Axios 是一个基于 promise 的 HTTP 库

NPM：包管理器

Babel：转码器

Webpack：打包工具

Docker	：容器技术

Git：代码管理工具 


#### 安装教程

1.  前端开发使用VSCode
前端直接将代码导入软件，然后依次运行
npm install
npm run
即可。
2.  后端开发使用Idea
后端删除了部分个人配置，比如中间件、短信等，更换为自己的配置即可。

#### 使用说明

1.  集成了微信支付以及微信登录
2.  集成了阿里云短信功能。包括短信登录、预约成功短信提醒、取消预约短信提醒、就医短信提醒。

