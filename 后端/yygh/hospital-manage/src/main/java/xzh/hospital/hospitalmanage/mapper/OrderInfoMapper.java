package xzh.hospital.hospitalmanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xzh.hospital.hospitalmanage.model.OrderInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}
