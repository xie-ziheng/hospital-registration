package xzh.hospital.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xzh.hospital.model.order.PaymentInfo;

public interface PaymentMapper extends BaseMapper<PaymentInfo> {
}
