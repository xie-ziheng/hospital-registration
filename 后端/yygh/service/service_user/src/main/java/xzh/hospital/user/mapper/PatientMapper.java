package xzh.hospital.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xzh.hospital.model.user.Patient;

public interface PatientMapper extends BaseMapper<Patient> {
}
