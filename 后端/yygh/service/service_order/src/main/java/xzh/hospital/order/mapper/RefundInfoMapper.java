package xzh.hospital.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xzh.hospital.model.order.RefundInfo;

public interface RefundInfoMapper extends BaseMapper<RefundInfo> {
}
